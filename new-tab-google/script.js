var searchBar = document.getElementById("searchBar");
searchBar.addEventListener("focus", clearBar);
searchBar.addEventListener("blur", addPlaceHolder);
searchBar.addEventListener("keyup", search);

function clearBar() {
  searchBar.placeholder = "";
}

function addPlaceHolder() {
  searchBar.placeholder = "Search Google or type URL";
}

function search(event) {
  if (event.key === "Enter") {
    var query = searchBar.value;
    window.location = "https://www.google.co.in/search?q=" + query;
    searchBar.value="";
  }
}

document.getElementById("appsIcon").addEventListener("click",showApps);
document.getElementById("bellIcon").addEventListener("click",showNotifs);
document.getElementById("userIcon").addEventListener("click",showAccount);
document.body.addEventListener("click",removeMenus);

function showApps()
{
  console.log("called")
  document.getElementById("notifMenu").style.visibility="hidden";
  document.getElementById("accMenu").style.visibility="hidden";
  document.getElementById("appsMenu").style.visibility="visible";
}

function showNotifs()
{
  console.log("called notif")
  document.getElementById("accMenu").style.visibility="hidden";
  document.getElementById("appsMenu").style.visibility="hidden";
  document.getElementById("notifMenu").style.visibility="visible";
}

function showAccount()
{
  console.log("called acc")
  document.getElementById("appsMenu").style.visibility="hidden";
  document.getElementById("notifMenu").style.visibility="hidden";
  document.getElementById("accMenu").style.visibility="visible";

}

function removeMenus(event)
{
  var menuHeaders = '.showMenu *';
  if (event.target.matches(menuHeaders)) {
    return
  }
  else{
  document.getElementById("appsMenu").style.visibility="hidden";
  document.getElementById("notifMenu").style.visibility="hidden";
  document.getElementById("accMenu").style.visibility="hidden";
}
}
