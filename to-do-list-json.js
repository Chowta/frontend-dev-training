// sending data

{
    "task_name" : "Clean bag",
    "completed" : true,
    "priority" : 2
}

// receiving data

{
    "tasks" : [
      {
        "task_name":"Clean shoes",
        "completed":true,
        "priority":2
      },
      {
         "task_name":"Workout",
        "completed":false,
        "priority":1
      },
      {
         "task_name":"Reply to emails",
        "completed":true,
        "priority":3
      }
      ]
  }