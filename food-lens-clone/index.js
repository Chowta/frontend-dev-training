new WOW().init();
window.onscroll = function() {showStickynav()};
var nav = document.querySelector("nav");
var sticky = nav.offsetTop-10;

function showStickynav() {
  if (window.pageYOffset > sticky) {
    nav.classList.add("sticky");
    document.querySelector("#searchSection").classList.add("stick");
    document.querySelector("#logoSection").classList.add("stick");

  } else {
    nav.classList.remove("sticky");
    document.querySelector("#searchSection").classList.remove("stick");
    document.querySelector("#logoSection").classList.remove("stick");

  }
}