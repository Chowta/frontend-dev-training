//IMMEDIATELY INVKLING FUNCTION EXPRESSION
/*var LOGINMODULE=function(){
var loginBtn = "#loginBtn", signinBtn ="#signinBtn",loginModal ="#loginModal", modalContent="#modalContent", failureMessage = "div p", welcomeMsg ="#welcomeMsg", username = document.querySelector("#usernameInput"), phoneNum = document.querySelector("#phoneNumInput");

function displayModal(event) {
  event.stopPropagation();
  displayer({[loginModal]:"block"});
}

function displayer(elements){
  for(var el in elements)
  {
    document.querySelector(el).style.display=elements[el];
  }
}

function removeModal() {
    displayer({[failureMessage]:"none",[loginModal]:"none"});
    username.value = "";
    phoneNum.value = "";
}

function dontClose(event){
event.stopPropagation();
}

function validate(event) {
  if (
    (!username.value.trim == "") &&
    (!isNaN(phoneNum.value)) &&
    (phoneNum.value.length === 10)
  ) {
    displayer({[loginModal]:"none",[loginBtn]:"none",[failureMessage]:"none",[welcomeMsg]:"block"});
    document.querySelector(welcomeMsg).innerHTML += username.value + "!";
  } else {
    displayer({[failureMessage]:"block"});
  }
}

return{
  loginBtn:document.querySelector(loginBtn),
  signinBtn:document.querySelector(signinBtn),
  modalContent:document.querySelector(modalContent),
  displayModal:displayModal,
  validate:validate,
  removeModal:removeModal,
  dontClose:dontClose
}
}();
 LOGINMODULE.loginBtn.addEventListener("click", LOGINMODULE.displayModal);
LOGINMODULE.signinBtn.addEventListener("click", LOGINMODULE.validate);
document.body.addEventListener("click", LOGINMODULE.removeModal);
LOGINMODULE.modalContent.addEventListener("click",LOGINMODULE.dontClose)*/

//IMMEDIATELY INVOKING OBJECT EXPRESSION

var LOGINMODULE = {
  loginBtn: document.querySelector("#loginBtn"),
  signinBtn: document.querySelector("#signinBtn"),
  loginModal: document.querySelector("#loginModal"),
  modalContent: document.querySelector("#modalContent"),
  failureMessage: document.querySelector("div p"),
  welcomeMsg: document.querySelector("#welcomeMsg"),
  username: document.querySelector("#usernameInput"),
  phoneNum: document.querySelector("#phoneNumInput"),

  displayer: function(elements) {
    for (i = 0; i < elements.length; i++) {
      elements[i].element.style= elements[i].style;
    }
  },

  displayModal: function() {
    event.stopPropagation();
    this.displayer([{element: this.loginModal, style: "display:block;" }]);
  },

  removeModal: function() {
    this.displayer([{element:this.failureMessage,style: "display:none"}, {element:loginModal,style:"display:none"} ]);
    this.username.value = "";
    this.phoneNum.value = "";
  },

  dontClose: function(event) {
    event.stopPropagation();
  },

  validate: function() {
    if (
      !(this.username.value.trim() === "") &&
      !(isNaN(this.phoneNum.value)) &&
      (this.phoneNum.value.length === 10)
    ) {
      this.displayer([{element:this.loginBtn,style:"display:none;"},{element:this.failureMessage,style:"display:none;"},{element:this.loginModal,style:"display:none;"},{element:this.welcomeMsg,style:"display:block;"}]);
      this.welcomeMsg.innerHTML += this.username.value + "!";
    } else {
      this.displayer([{element:this.failureMessage,style:"display:block;"}]);
    }
  },

  main: function() {
    loginBtn.addEventListener("click", this.displayModal.bind(this));
    signinBtn.addEventListener("click", this.validate.bind(this));
    document.body.addEventListener("click", this.removeModal.bind(this));
    modalContent.addEventListener("click", this.dontClose.bind(this));
  }
}.main();
