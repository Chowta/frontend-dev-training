var srcData;
var submitted = false;
var projectModal = document.getElementById("projectModal");
var internshipModal = document.getElementById("internshipModal");

function showInternshipModal() {
  internshipModal.style.display = "block";
}

function showProjModal() {
  projectModal.style.display = "block";
}

function removeModal(event) {
  if (event.target == projectModal) {
    projectModal.style.display = "none";
  }
  if (event.target == internshipModal) {
    internshipModal.style.display = "none";
  }
}

document
  .getElementById("addInternshipBtn")
  .addEventListener("click", showInternshipModal);
document.getElementById("addProjBtn").addEventListener("click", showProjModal);
window.addEventListener("click", removeModal);
document
  .getElementById("internshipModalBtn")
  .addEventListener("click", addInternship);
document.getElementById("projModalBtn").addEventListener("click", addProject);
document.getElementById("submit").addEventListener("click",onSubmitForm)

var closeInternshipBtn = document.getElementById("closeInternshipBtn");
closeInternshipBtn.onclick = function() {
    internshipModal.style.display = "none";
}

var closeProjectBtn = document.getElementById("closeProjectBtn");
closeProjectBtn.onclick = function() {
    projectModal.style.display = "none";
}

function addInternship() {
  internshipModal.style.display = "none";
  var internship = document.getElementById("internship");
  var internshipTitle = document.getElementById("internshipTitle").value;
  var internshipOrganisation = document.getElementById("internshipOrganisation")
    .value;
  var internshipDescription = document.getElementById("internshipDescription")
    .value;
  document.getElementById("internshipTitle").value = "";
  var li = document.createElement("li");
  li.appendChild(
    document.createTextNode(
      internshipTitle +
        ", at " +
        internshipOrganisation +
        ": " +
        internshipDescription
    )
  );
  internship.appendChild(li);
  document.getElementById("internshipTitle").value = "";
  document.getElementById("internshipOrganisation").value = "";
  document.getElementById("internshipDescription").value = "";
  if(submitted===false)
    alert("Added! Press submit to view in resume");
}

function addProject() {
  projectModal.style.display = "none";
  var project = document.getElementById("project");
  var projectTitle = document.getElementById("projectTitle").value;
  var projectDescription = document.getElementById("projectDescription").value;
  var li = document.createElement("li");
  li.appendChild(
    document.createTextNode(projectTitle + ": " + projectDescription)
  );
  project.appendChild(li);
  document.getElementById("projectTitle").value = "";
  document.getElementById("projectDescription").value = "";
  if(submitted===false)
    alert("Added! Press submit to view in resume");
}

function onSubmitForm(e) {
  if (submitted === false) {
    document.getElementById("submittedDetails").style.display = "block";
    e.preventDefault();
    document.getElementById(
      "submittedName"
    ).innerHTML = document.getElementById("name").value;
    document.getElementById(
      "submittedEmail"
    ).innerHTML = document.getElementById("email").value;
    var addr = document.getElementById("address").value;
    addr = addr.replace(/\r?\n/g, "<br>");
    document.getElementById("submittedAddress").innerHTML = addr;
    document.getElementById("submittedPh").innerHTML =
      "Contact: " + document.getElementById("phoneNum").value;
    var dropdown = document.getElementById("graduation");
    document.getElementById("submittedGradCourse").innerHTML =
      "B.E. in " + dropdown.options[dropdown.selectedIndex].text;
    document.getElementById(
      "submittedGradInstitute"
    ).innerHTML = document.getElementById("gradSchool").value;
    document.getElementById("submittedCGPA").innerHTML =
      document.getElementById("cgpa").value + " CGPA";
    document.getElementById(
      "submitted12thInstitute"
    ).innerHTML = document.getElementById("12thschool").value;
    document.getElementById("submitted12thPercentage").innerHTML =
      document.getElementById("12thmarks").value + "%";
    document.getElementById(
      "submitted10thInstitute"
    ).innerHTML = document.getElementById("10thschool").value;
    document.getElementById("submitted10thPercentage").innerHTML =
      document.getElementById("10thmarks").value + "%";
    var elems = document.querySelectorAll("[name=programmingLangs]");
    for (var i = 0; i < elems.length; i++) {
      if (elems[i].checked) {
        var li = document.createElement("li");
        li.appendChild(document.createTextNode(elems[i].value));
        document.getElementById("programmingLanguages").appendChild(li);
      }
    }
    var elems = document.querySelectorAll("[name=db]");
    for (var i = 0; i < elems.length; i++) {
      if (elems[i].checked) {
        var li = document.createElement("li");
        li.appendChild(document.createTextNode(elems[i].value));
        document.getElementById("databases").appendChild(li);
      }
    }
    if (document.getElementById("fileInput").files[0]) {
      document.getElementById("profilePic").src =
        "Assets/ProfilePictures/" + document.getElementById("fileInput").files[0].name;
    }
    submitted = true;
  }
  else
    alert('Already submitted');
}

$("#downloadPdfBtn").click(function() {
  $("html, body").scrollTop(0);
  html2canvas($("#pdfContent"), {
    onrendered: function(canvas) {
      var img = canvas.toDataURL("image/png");
      var doc = new jsPDF();
      doc.addImage(img, "JPEG", 15, 15);
      doc.save("resum.pdf");
    }
  });
});

$('.yearselect').yearselect();
$('.yearselect').yearselect({
      start: 2000,
    
      end: 2016
    
    });
    
    $('.yearselect').yearselect({
        
          order: 'asc' // or desc
        
        });
        
        $('.yearselect').yearselect({
            
              selected: 2016
            
            });
            


