var MODULE = (function() {
  // initialize carousel

  $(document).ready(function() {
    displayLogos();
    $(".carousel-container").slick({
      dots: true,
      infinite: true,
      autoplay: true,
      infinite: true,
      speed: 200,
      slidesToShow: 1,
      adaptiveHeight: true,
      pauseOnHover: true
    });
  });

  function displayLogos(){
    Array.from(document.querySelectorAll("#intro img")).forEach(function(element)
    {
      element.style.margin="1% 4%";
    })
  }

  // smooth scrolling

  function scrollContent(e) {
    e.preventDefault();
    $("body,html").animate(
      {
        scrollTop: $(this.hash).offset().top
      },
      800
    );
  }

  // converting nodelist returned by querySelectorAll to array
  var links = Array.from(document.querySelectorAll(".smooth-scroll-link"));
  links.forEach(function(element) {
    element.addEventListener("click", scrollContent.bind(element));
  });

  document.querySelector("nav .icon").addEventListener("click", toggle);

  function toggle(event) {
    event.preventDefault();
    var nav = document.querySelector("header nav");
    var hdr = document.querySelector("header");
    if (nav.classList.contains("responsive")) {
      nav.classList.remove("responsive");
      hdr.classList.remove("responsive");
    } else {
      nav.classList.add("responsive");
      hdr.classList.add("responsive");
    }
  }
})();
